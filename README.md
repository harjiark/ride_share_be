
  
Description: 

This application is a simple ride-share backend system. Using rest-api gem to provide restfull json api and mysql as database. And we using gcm gem for the part of push message notification. All message is in json format. 

Restriction:

Only support push message for android device. 

Environment details: 

  . jdk 1.7
  . jruby 9.0.5
  . rails 4.2.4
  . mysql 5.6.31

Configuration:
See database.yml file


How to start: 
In your root project 
 
  1. bundle install (fetch dependencies)
  2. rake db:drop db:create db:migrate db:seed (migration and seed database) 
  3. rails s (star the server)



Urls: 
    Prefix Verb    URI Pattern                  Controller#Action
         rides POST    /rides(.:format)             rides#create
          ride PATCH   /rides/:id(.:format)         rides#update
               PUT     /rides/:id(.:format)         rides#update
      requests GET     /requests(.:format)          requests#index
               POST    /requests(.:format)          requests#create
   new_request GET     /requests/new(.:format)      requests#new
  edit_request GET     /requests/:id/edit(.:format) requests#edit
       request GET     /requests/:id(.:format)      requests#show
               PATCH   /requests/:id(.:format)      requests#update
               PUT     /requests/:id(.:format)      requests#update
               DELETE  /requests/:id(.:format)      requests#destroy
         users POST    /users(.:format)             users#create
               OPTIONS /*path(.:format)             application#cors_preflight
          auth POST    /auth(.:format)              authentications#create
               DELETE  /auth(.:format)              authentications#destroy
         order POST    /order(.:format)             requests#create
               GET     /order(.:format)             requests#show
request_action PATCH   /request/action(.:format)    requests#action
   rides_start POST    /rides/start(.:format)       rides#create
     rides_end PATCH   /rides/end(.:format)         rides#update
          root GET     /                            users#index


Example message: 
Valid message will reply with response code 201, error message with 401
1. Registering user 
    
    http method: POST

    Access-Control-Allow-Headers →*
	Access-Control-Allow-Methods →GET, POST, PATCH, PUT, DELETE, OPTIONS
    Access-Control-Allow-Origin →http://localhost:8888
    Access-Control-Max-Age →172800
    Cache-Control →max-age=0, private, must-revalidate
    Connection →Keep-Alive
    Content-Length →209
    Content-Type →application/json; charset=utf-8
    Date →Tue, 08 Nov 2016 04:16:28 GMT
    Etag →W/"7319482aea5295f5404a478ed521670c"
    Server →WEBrick/1.3.1 (Ruby/2.2.3/2016-01-26)
    X-Content-Type-Options →nosniff
    X-Frame-Options →SAMEORIGIN
    X-Request-Id →70b1fa5a-d338-482b-9890-fa8b65c11765
    X-Runtime →0.684757
    X-Xss-Protection →1; mode=block
    
    Url: /users
    Content-Type: application/json

    Request Message: 
    {
    "user":{
        "name":"harji3",
        "email":"harji3@gmail.com",
    	"password":"harji",
    	"password_confirmation":"harji",
    	"phone_number":"089191919191",
    	"registration_device_id": "293843", #gcm client id 
    	"device_type": "android"
    	"user_type" : "passenger" (passenger/driver)
     }
  }

  response: 
  
  {
  "id": 1,
  "access_token": "1a72451f2c8c5c208f12b46b50c5dbe8",
  "activity_type": null,
  "user": {
    "id": 4,
    "name": "harji4",
    "phone_number": "089191991",
    "user_type": {
      "id": 2,
      "name": "passenger",
      "description": "user passenger"
      }
    }
  }


2. authentication : 
   
   http method = POST
   url : /auth
   Request: 
   {
      "phone_number" : "089191991",
      "password" : "harji"
   }

   response: 

   {
   	"id": 1,
   	"access_token": "1a72451f2c8c5c208f12b46b50c5dbe8",
   	"activity_type": {
   	  "id": 2,
   	  "name": "online",
   	  "description": "user online"
   	},
  	"user": {
  	  "id": 4,
  	  "name": "harji4",
  	  "phone_number": "089191991",
 	 	  "user_type": {
 	 	    "id": 2,
 	 	    "name": "passenger",
 	     "description": "user passenger"
  	  	}
 		 }
	  }

3. Request order
   http method = POST
   add X-ACCESS-TOKEN = <access_token> in the header, get access_token from authentication request
   example:
   X-ACCESS-TOKEN= 1a72451f2c8c5c208f12b46b50c5dbe8

   url -> /order
   request :
   {
  "request": {
    
      "source": {
        "longitude": "498343343",
        "latitude": "893430484"
      
      },
      "destination": {
        "longitude": "498343343",
        "latitude": "893430484"
      
      }
  	}
	}

	response:

	{
  "id": 9,
  "requester": {
    "id": 4,
    "name": "harji4",
    "email": "harji34gmail.com"
  },
  "driver": null,
  "status": "onprogress",
  "locations": [
    {
      "id": 17,
      "longitude": 498343343,
      "latitude": 893430484,
      "description": null
    },
     {
      "id": 18,
      "longitude": 498343343,
      "latitude": 893430484,
      "description": null
     }
   ]
  }

4. Driver accept order -> user driver login and receive order from passenger 
   set additional toke header

   http method : PATCH
   X-ACCESS-TOKEN= 303846a93910a49c391e1504f672ef42 -> token of driver login
   url : /requests/action
   
   message request: 

   {
    "driver_action":{
    "request_id" : "3",
    "acction_status"    : "accept"
    
    }
   }

   message response: 
   {
  "id": 9,
  "requester": {
    "id": 4,
    "name": "harji4",
    "email": "harji34gmail.com"
  },
  "driver": {
    "id": 2,
    "name": "Bob",
    "email": "bob@gmail.com"
  },
  "status": "accept",
  "locations": [
    {
      "id": 17,
      "longitude": 498343343,
      "latitude": 893430484,
      "description": null
    },
    {
      "id": 18,
      "longitude": 498343343,
      "latitude": 893430484,
      "description": null
    	}
   	]
   }

  5. Driver update location



  6. Driver ride start
     method : POST
     url: /rides/start
     request :
     X-ACCESS-TOKEN= 303846a93910a49c391e1504f672ef42 -> token of driver login

     {
      "ride":{
     	"request_id" : 9,
     	"action" : "start",
     	"ride_type" : "start"
        }
     }

     response: 
     {

  "id": 1,
  "start_time": "2016-11-08T09:32:45.626Z",
  "arrived_time": null,
  "request": {
    "id": 9,
    "requester": {
      "id": 4,
      "name": "harji4",
      "email": "harji34gmail.com"
    },
    "driver": {
      "id": 2,
      "name": "Bob",
      "email": "bob@gmail.com"
    },
    "status": "accept",
    "locations": [
      {
        "id": 17 ,
        "longitude": 498343343,
        "latitude": 893430484,
        "description": null
      },
      {
        "id": 18,
        "longitude": 498343343,
        "latitude": 893430484,
        "description": null
      }
    ]
  }
}
 

 6. Driver ride stop
     http method : PATCH
     url: /rides/end
     request :
     X-ACCESS-TOKEN= 303846a93910a49c391e1504f672ef42 -> token of driver login

     {
      "ride":{
     	"request_id" : 9,
     	"action" : "stop",
     	"ride_type" : "start"
        }
     }

 7. Driver update location
    X-ACCESS-TOKEN= 303846a93910a49c391e1504f672ef42 -> token of driver login
    http method : PATCH
    url: /rides/end

    http method :PATCH
    {
      location:{
      longitude:09332,
      latitude :293487
      }
    }


  Assumtion. Android application should fetch and store gcm_id.  

    