# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161103064325) do

  create_table "activities", force: :cascade do |t|
    t.string   "access_token",     limit: 255
    t.integer  "user_id",          limit: 4
    t.integer  "activity_type_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "scope",            limit: 50
    t.datetime "expires_at"
    t.datetime "last_access"
    t.boolean  "is_locked"
    t.string   "activity_type",    limit: 255
  end

  add_index "activities", ["activity_type_id"], name: "index_activities_on_activity_type_id", using: :btree
  add_index "activities", ["user_id"], name: "index_activities_on_user_id", using: :btree

  create_table "activity_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "activity_types", ["name"], name: "index_activity_types_on_name", unique: true, using: :btree

  create_table "location_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "location_types", ["name"], name: "index_location_types_on_name", unique: true, using: :btree

  create_table "locations", force: :cascade do |t|
    t.decimal  "longitude",                      precision: 10
    t.decimal  "latitude",                       precision: 10
    t.text     "description",      limit: 65535
    t.integer  "location_type_id", limit: 4
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "request_id",       limit: 4
  end

  add_index "locations", ["location_type_id"], name: "index_locations_on_location_type_id", using: :btree
  add_index "locations", ["request_id"], name: "fk_rails_fb3d642971", using: :btree

  create_table "request_statuses", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "request_statuses", ["name"], name: "index_request_statuses_on_name", unique: true, using: :btree

  create_table "requests", force: :cascade do |t|
    t.integer  "requester_id",       limit: 4
    t.integer  "driver_id",          limit: 4
    t.integer  "request_status_id",  limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "driver_location_id", limit: 4
    t.string   "status",             limit: 255
  end

  add_index "requests", ["request_status_id"], name: "index_requests_on_request_status_id", using: :btree

  create_table "ride_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "ride_types", ["name"], name: "index_ride_types_on_name", unique: true, using: :btree

  create_table "rides", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "arrived_time"
    t.integer  "request_id",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "ride_type_id", limit: 4
  end

  add_index "rides", ["request_id"], name: "index_rides_on_request_id", using: :btree
  add_index "rides", ["ride_type_id"], name: "fk_rails_ce8fbebd21", using: :btree

  create_table "user_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "user_types", ["name"], name: "index_user_types_on_name", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "id_num",                 limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "user_type_id",           limit: 4
    t.string   "phone_number",           limit: 255
    t.string   "password_digest",        limit: 255
    t.string   "email",                  limit: 255
    t.string   "registration_device_id", limit: 255
    t.string   "device_tpye",            limit: 255
  end

  add_index "users", ["phone_number"], name: "index_users_on_phone_number", unique: true, using: :btree
  add_index "users", ["user_type_id"], name: "fk_rails_a2f1461231", using: :btree

  add_foreign_key "activities", "activity_types"
  add_foreign_key "activities", "users"
  add_foreign_key "locations", "location_types"
  add_foreign_key "locations", "requests"
  add_foreign_key "requests", "request_statuses"
  add_foreign_key "rides", "requests"
  add_foreign_key "rides", "ride_types"
  add_foreign_key "users", "user_types"
end
