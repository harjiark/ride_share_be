# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def User.digest(string)
  cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                BCrypt::Engine.cost
  BCrypt::Password.create(string, cost: cost)
end

usertypes = UserType.create([{ name: 'driver', description: 'user driver'}, { name: 'passenger', description: 'user passenger'}])

User.create([{ name: 'Alice', id_num: '098980989', user_type_id: usertypes[1][:id], phone_number: '08996969035', email: 'alice@gmail.com', password_digest: User.digest('alice'), registration_device_id: '123450' },
	{ name: 'Bob', id_num: '77777', user_type_id: usertypes[0][:id], phone_number: '08996969036', email: 'bob@gmail.com', password_digest: User.digest('bob'), registration_device_id: '123451' }]);


# bob = User.new(name: 'Bob', id_num: '77777', user_type: usertypes[0], phone_number: '08996969036')
# alice = User.new(name: 'Alice', id_num: '098980989', user_type: usertypes[1], phone_number: '08996969035')


# # User.find_or_create_by(alice)
# # User.find_or_create_by(bob)
# alice.save
# bob.save

activity_type = ActivityType.create([{ name: 'offline', description: 'user offline'},
	{ name: 'online', description: 'user online'},
	{ name: 'request', description: 'passenger on request'},
	{ name: 'accept_request', description: 'driver accept request'},
	{ name: 'ride', description: 'driver on the way to deliver passenger'},
	{ name: 'available', description: 'driver available'}])

location_type = LocationType.create([{ name: 'source', description: 'initial user location'},
	{ name: 'destination', description: 'destination user location'},
	{ name: 'ontheway', description: 'on the way location'}])


ride_type = RideType.create([{ name: 'tostartpoint', description: 'ride from current position to passenger start position'},
	{ name: 'starttodest', description: 'ride from source to destination passenger'}])

# ActiveRecord::Base.establish_connection
# ActiveRecord::Base.connection.tables.each do |table|
#   next if table == 'schema_migrations'

#   # MySQL and PostgreSQL
#   ActiveRecord::Base.connection.execute("TRUNCATE #{table}")

#   # SQLite
#   # ActiveRecord::Base.connection.execute("DELETE FROM #{table}")
# end