class AddUnique < ActiveRecord::Migration
  def change
  	add_index :user_types, :name, :unique => true
    add_index :location_types, :name, :unique => true
    add_index :request_statuses, :name, :unique => true
  end
end
