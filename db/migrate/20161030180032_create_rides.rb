class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.datetime :start_time
      t.datetime :arrived_time
      # t.references :location, index: true, foreign_key: true
      t.references :request, index: true, foreign_key: true

      t.timestamps null: false
    end

    add_column :requests, :driver_location_id, :integer
  end
end
