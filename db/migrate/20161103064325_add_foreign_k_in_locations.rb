class AddForeignKInLocations < ActiveRecord::Migration
  def change
  	remove_column :requests , :source_id 
  	remove_column :requests , :destination_id
    # remove_column :rides , :location_id
  	add_column :locations, :request_id, :integer
  	add_column :requests, :status, :string

  	add_column :users, :device_tpye, :string
  	
  	# rename_column :users, :gcm_code, :registration_device_id
    
    add_column :activities, :activity_type, :string

  	add_foreign_key :locations, :requests
  	
  end
end
