class Addridetypeforeignkey < ActiveRecord::Migration
  def change
  	add_column :rides, :ride_type_id, :integer
  	add_foreign_key :rides, :ride_types
  end
end
