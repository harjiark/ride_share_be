class CreateRideTypes < ActiveRecord::Migration
  def change
    create_table :ride_types do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
    add_index :ride_types, :name, unique: true
    
  end
end
