class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.decimal :longitude
      t.decimal :latitude
      t.text :description
      t.references :location_type, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
