class AddNewFieldOnActivityTable < ActiveRecord::Migration
  def change
  	add_column("activities", "scope", :string, :limit => 50)
  	add_column("activities", "expires_at", :datetime)
  	add_column("activities", "last_access", :datetime)
  	add_column("activities", "is_locked", :boolean)
  	add_column("users", "password_digest", :string)
  	add_column("users", "email", :string)
  	add_column("users", "registration_device_id", :string,:unique =>true)
  	# add_column :users, :user_type, :string, :unique =>true
  	# rename_column :activities, :tokden_id, :access_token
    change_column :users, :id_num, :string
  end
end
