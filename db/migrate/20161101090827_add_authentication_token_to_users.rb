class AddAuthenticationTokenToUsers < ActiveRecord::Migration
  def change
    # add_column :users, :authentication_token, :string, limit: 30
    add_column :users, :phone_number, :string 
    add_index :users, :phone_number, unique: true
    # add_index :users, :authentication_token, unique: true
  end
end
