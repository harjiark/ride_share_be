class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.integer :requester_id, :references => "user" 
      t.integer :driver_id, :references => "user"
      t.integer :source_id, :references => "location"
      t.integer :destination_id, :references => "location"
      t.references :request_status, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
