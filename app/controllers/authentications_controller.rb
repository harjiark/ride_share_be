class AuthenticationsController < ApplicationController

	skip_before_filter :ensure_authenticated_user, :only => [:create]

  # Logging the user in
  #implement it in ride share app and remove the password authentication
  def create
  	user = User.where("phone_number =  ?", params[:phone_number]).first

  	if user && user.authenticate(params[:password])
  		api_key = user.find_api_key

  		if !api_key.is_locked
  			api_key.last_access = Time.now

  			if !api_key.access_token || api_key.is_expired?
  				puts "non existant or expired key, generating"
  				api_key.set_expiry_date
  				api_key.generate_access_token
  			end

  			api_key.save
  			
  			activity_type = user.user_type.name == 'driver' ? ActivityType.find_by_name('available') : ActivityType.find_by_name('online')
  			
  			api_key[:activity_type_id]  = activity_type[:id]

        # api_key(:activity_type => activity_type.name)

  			p api_key
  			api_key.save
  			render json: api_key, status: 201
  		else
  			render json: { errors: 'Your account has been locked.' }, status: 401
  		end
  	else
  		render json: { errors: 'Could not authenticate properly.' }, status: 401
  	end
    
  end

  # Clearing user key when they log out
  def destroy

  	api_key = Activity.where(tokden_id: token).first

  	api_key.access_token = ''
  	api_key.expires_at = Time.now

  	if api_key.save
  		render json: {}, status: 200
  	else
  		render json: {}, status: 422
  	end

  end

end
