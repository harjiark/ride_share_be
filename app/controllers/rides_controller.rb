class RidesController < ApplicationController
  before_action :set_ride, only: [:show, :edit, :update, :destroy]

  before_filter :ensure_authenticated_user
  # GET /rides
  # GET /rides.json
  def index
    @rides = Ride.all
  end

  # GET /rides/1
  # GET /rides/1.json
  def show
  end

  # GET /rides/new
  def new
    @ride = Ride.new
  end

  # GET /rides/1/edit
  def edit
  end

  # POST /rides
  # POST /rides.json
  def create

    if current_user.user_type.name = 'driver'

      @ride = Ride.new(:request_id => ride_params[:request_id], :start_time => Time.now )
      
      @ride.save
      render json: @ride 
    else
      render json: { errors: 'cannot process this action' }, status: 401      
    end

    return 
    # @ride.ride_type = RideType.find_by_name(ride_params[:ride_type])  
    # respond_to do |format|

    #   if @ride.save
       
    #     # format.html { redirect_to @ride, notice: 'Ride was successfully created.' }
    #     format.json { render :show, status: :created, location: @ride }
    #   else
    #     # format.html { render :new }
    #     format.json { render json: @ride.errors, status: :unprocessable_entity }
    #   end
    # end

  end

  # PATCH/PUT /rides/1
  # PATCH/PUT /rides/1.json
  def update
    
   
    if current_user.user_type.name = 'driver'

       ride = Ride.find(ride_params[:id]) 
       # Ride.where(:request_id => ride_params[:request_id])
       ride.arrived_time = Time.now
       ride.save

      render json: ride 
    else
      render json: { errors: 'cannot process this action' }, status: 401      
    end

    return
    
  end

  # DELETE /rides/1
  # DELETE /rides/1.json
  def destroy
    @ride.destroy
    respond_to do |format|
      format.html { redirect_to rides_url, notice: 'Ride was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ride
      @ride = Ride.find(ride_params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ride_params
      params.require(:ride).permit(:id,:ride_type, :request_id, :action)
    end


  end
