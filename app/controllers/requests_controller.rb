  require 'service'



  class RequestsController < ApplicationController

    # before_action :set_request, only: [:show, :edit, :update, :destroy]
    before_filter :ensure_authenticated_user

    # GET /requests
    # GET /requests.json
    def index
      @requests = Request.all
    end


    # POST /requests
    # POST /requests.json
    def create

      if current_user.user_type.name == 'driver'
       render json: { errors: 'Invalid user type' }, status: 401
       return
     end

     requests = Request.where(:requester_id => current_user.id).where(:status => 'onprogress') 
     
     if requests.size > 0
       render json: { errors: 'other request on progress' }, status: 401
       return
     end
     
     if current_user

      p 'request_params:',request_params
      requestparams = request_params.clone
      @request = Request.new

      @request[:requester_id] = current_user[:id]
      @request.status = 'onprogress'
      @request.save

      source_type = LocationType.find_by_name('source')
      destination_type = LocationType.find_by_name('destination')
      ontheway_type = LocationType.find_by_name('ontheway')
       # p 'is nil:' , (request_params == nil)
       p 'requests:', @request
       p 'request_param:'+ request_params.to_s
       # binding.pry

       source = Location.create(location_type: source_type, 
        latitude: request_params[:source][:latitude],
        longitude: request_params[:source][:longitude],
        request_id: @request[:id])
       source.save

       destination = Location.create(location_type: destination_type, 
        latitude: request_params[:destination][:latitude],
        longitude: request_params[:destination][:longitude], 
        request_id: @request[:id])
       destination.save 

         
        drivers  = User.where(' user_type_id = ? and (id IN (select user_id from activities where activity_type_id = ? or activity_type_id = ?)) ',
          UserType.find_by_name('driver')[:id],
          ActivityType.find_by_name('available')[:id],
          ActivityType.find_by_name('online')[:id]
          )
         
         # driver_activities = Activity.where("user_id IN (?) and (activity_type_id=? or activity_type_id=? ) ",
         #  User.where(:user_type_id => UserType.find_by_name('driver')[:id]).pluck(:id),
         #  ActivityType.find_by_name('available'),
         #  ActivityType.find_by_name('online'))
         
         require 'service'

         drivers.each do|driver|
           Service::PushService.notify_android( @request, driver.registration_device_id, nil)
         end
        
         render json: @request, status: 201 
         
         return 
       else

         render json: { errors: 'wrong user token.' }, status: 401
         return

       end

     end

     def show
      render json: Request.where(:status =>'onprogress')
    end

  #  def edit
  #   request_status = request_params[:update_status]
  # end
  # PATCH
  def updateloc

    if user_type == 'driver'

     last_location = Location.where(:request_id => request_params[:request_id]).where(:location_type => LocationType.find_by_name('ontheway'))
     last_location[:longitude => location[:longitude], :latitude => location[:latitude]]
     render json: @request, status: 201 

   else

     render json: { errors: 'wrong user token.' }, status: 401
     

   end
   return
 end


 def action

  if current_user.user_type.name == 'driver'

       #TODO => implement gcm push 
       p 'reqid:'+action_params[:request_id]  
       request = Request.find(action_params[:request_id]) 
       request.status = action_params[:acction_status]
       request.driver_id = current_user.id
       request.save
       # current_user.activity.activity_type = Activity.find_by_name('accept_request')
       
       render json: request        
     else

      render json: { errors: 'cannot process this action' }, status: 401      
    end

    return
  end


  private

  def request_params
    params.require(:request).permit( :source => [:longitude, :latitude], :destination => [:longitude, :latitude])
  end

  def action_params
    params.require(:driver_action).permit(:request_id,:acction_status)
  end

  def locat_params
    params.require(:location).permit(:longitude,:latitude)
  end

  def user_type
    current_user.user_type.name
  end

end
