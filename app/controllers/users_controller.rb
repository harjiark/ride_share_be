class UsersController < ApplicationController
  # before_action :set_user, only: [:show, :edit, :update, :destroy]

  # acts_as_token_authentication_handler_for User
 # creating a user doesn't require you to have an access token
 skip_before_filter :ensure_authenticated_user, :only => [:create]

   # Returns the authenticated user information
   def show
    render json: @currentUser
  end

  # Create a new user, no authentication required
  def create
    # deny user creation if a user is logged in

    if current_user
      render json: { errors: "You are currently logged in." }, status: 401
    else
     userparams = user_params.clone 
     userparams.delete :user_type 
     

     p "user_params",user_params
    
     user = User.create(userparams)


     if !user.new_record?

      if user_params.has_key?(:user_type) && user_params.has_key?(:registration_device_id)
        user_type = UserType.find_by_name(user_params[:user_type])
        p "user_type: ",user_type
        user["user_type_id"] = user_type.id
        user.save
        render json: user.find_api_key, status: 201
      else
        render json: { errors: "Mising some parameters" }, status: 401
      end

    else
      render json: { errors: user.errors.messages }, status: 422
    end

  end
end

  # Update the authenticated user profile
  def update
    # require password to change account information
    if @currentUser && @currentUser.authenticate(user_params[:password])

      updatedParams = user_params

      updatedParams[:password] = updatedParams[:new_password]
      updatedParams.delete(:new_password)

      if @currentUser.update_attributes(updatedParams)
        head :no_content
      else
        render json: { errors: @currentUser.errors.messages }, status: 422
      end
    else
      render json: { errors: @currentUser.errors.messages }, status: 401
    end
  end

  private

  # Strong Parameters (Rails 4)
  def user_params
    p params
    # binding.pry
    params.require(:user).permit(:name, :email,:phone_number, :password, :password_confirmation, :user_type,:registration_device_id)

    # params.require(:email, :password, :password_confirmation, :phone_number, :user_type).permit(:name)
    # permit(:name, :username, :email, :password, :password_confirmation)
  end
end
