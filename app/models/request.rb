class Request < ActiveRecord::Base
# accepts_nested_attributes_for
  belongs_to :request_status

  belongs_to :requester, :class_name => 'User', :foreign_key => 'requester_id'
  belongs_to :driver, :class_name => 'User', :foreign_key => 'driver_id'

  has_many :locations

end
