class BaseType < ActiveRecord::Base
	self.abstract_class = true
	def as_json(options={})
    {
      :id => id,
      :name => name,
      :description => description
    }
  end
end