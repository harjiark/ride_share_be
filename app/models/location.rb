class Location < ActiveRecord::Base
  belongs_to :location_type
  belongs_to :requests

  def as_json(options={})
    {
      :id => id,
      :longitude => longitude,
      :latitude => latitude,
      :description => description
    
    }
  end

   # :id => id,
      # :name => name,
      # :description => description
      # :created_at => created_at,
      # :updated_at => updated_at
  
end
