class User < ActiveRecord::Base
	
  # include Mongoid::Document
  has_secure_password
  
  belongs_to :user_type
  has_many :activities

  validates :phone_number , presence: true, uniqueness: true
  validates :email , presence: true, uniqueness: true
  validates :name , presence: true 
  validates :registration_device_id , presence: true , uniqueness: true
  # validates :user_type_id, presence: true
  
  def find_api_key (targetScope = 'android-app')
    self.activities.where(scope: targetScope).first_or_create
  end

  def as_json(options={})
    {
      :id => id,
      :name => name,
      :email => email
    }
  end

  
end
