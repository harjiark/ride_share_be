class RideTypeSerializer < ActiveModel::Serializer
  attributes :id, :name, :description
end