class LocationSerializer < ActiveModel::Serializer
  attributes :id, :longitude, :latitude, :location_type, :description
end