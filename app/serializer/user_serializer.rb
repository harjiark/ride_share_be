class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :phone_number, :user_type

  # belongs_to :activity_type
end