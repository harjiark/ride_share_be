class ActivitySerializer < ActiveModel::Serializer
  attributes :id, :access_token , :activity_type
  has_one :user, embed: :id
end