json.extract! ride, :id, :start_time, :arrived_time, :location_id, :request_id, :created_at, :updated_at
json.url ride_url(ride, format: :json)