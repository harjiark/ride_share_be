json.extract! request, :id, :requester_id, :driver_id, :source_id, :destination_id, :request_status_id, :created_at, :updated_at
json.url request_url(request, format: :json)