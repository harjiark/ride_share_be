json.extract! activity, :id, :access_token, :user_id, :activity_type_id, :created_at, :updated_at
json.url activity_url(activity, format: :json)